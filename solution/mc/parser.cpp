#include "parser.hpp"

#include <iostream>

Parser::Parser(Symbol_table& syms, 
               std::string const& input)
  : m_lex(syms, input)
{
  // Pull all of the tokens in one shot.
  while (Token tok = m_lex.get_next_token())
    m_toks.push_back(tok);

  m_lookahead = m_toks.data();
  m_last = m_lookahead + m_toks.size();
}

Token 
Parser::consume()
{
  assert(!is_eof());
  Token ret = *m_lookahead;
  ++m_lookahead;
  return ret;
}

Token 
Parser::match(Token::Name n)
{
  if (next_token_is(n))
    return consume();
  return Token();
}

Token 
Parser::expect(Token::Name n)
{
  if (next_token_is(n))
    return consume();

  throw std::runtime_error("syntax error");
  return Token();
}

Token 
Parser::require(Token::Name n)
{
  assert(next_token_is(n));
  return consume();
}

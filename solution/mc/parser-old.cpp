#include "parser.hpp"

#include <iostream>

Parser::Parser(Symbol_table& syms, 
               std::string const& input)
  : m_lex(syms, input)
{
  // Pull all of the tokens in one shot.
  while (Token tok = m_lex.get_next_token())
    m_toks.push_back(tok);

  m_lookahead = m_toks.data();
  m_last = m_lookahead + m_toks.size();
}

Token 
Parser::consume()
{
  assert(!is_eof());
  Token ret = *m_lookahead;
  ++m_lookahead;
  return ret;
}

Token 
Parser::match(Token::Name n)
{
  if (next_token_is(n))
    return consume();
  return Token();
}

Token 
Parser::expect(Token::Name n)
{
  if (next_token_is(n))
    return consume();

  throw std::runtime_error("syntax error");
  return Token();
}

Token 
Parser::require(Token::Name n)
{
  assert(next_token_is(n));
  return consume();
}

void
Parser::parse_expression()
{
  parse_additive_expression();
}

/// Parse an expression.
///
///   additive-expression -> additive-expression '+' multiplicative-expression
///   additive-expression -> additive-expression '-' multiplicative-expression
///   additive-expression -> multiplicative-expression
///
/// Refactored:
///
///   additive-expression -> multiplicative-expression additive-expression-rest
void 
Parser::parse_additive_expression()
{
  parse_multiplicative_expression();

  // Step 4. Eliminate extra call.
  while (match(Token::plus) || match(Token::minus))
    parse_multiplicative_expression();
}

///   additive-expression-rest -> '+' multiplicative-expression additive-expression-rest
///   additive-expression-rest -> '-' multiplicative-expression additive-expression-rest
///   additive-expression-rest -> <empty>
void 
Parser::parse_additive_expression_rest()
{
  // Step3. Eliminated tail recursion.
  while (match(Token::plus) || match(Token::minus)) {
    parse_multiplicative_expression();
  }

  // Step 2. Make tail recursive.
  // if (!match(Token::plus) && !match(Token::minus))
  //   return;

  // parse_multiplicative_expression();
  // parse_additive_expression_rest();

  // Step 1.
  // if (match(Token::plus) || match(Token::minus)) {
  //   parse_multiplicative_expression();
  //   parse_additive_expression_rest();
  // }

  // Step 0.
  // if (match(Token::plus)) {
  //   parse_multiplicative_expression();
  //   parse_additive_expression_rest();
  //   return;
  // }
  // if (match(Token::minus)) {
  //   parse_multiplicative_expression();
  //   parse_additive_expression_rest();
  //   return;
  // }
}

///   multiplicative-expression -> multiplicative-expression '*' postfix-expression
///   multiplicative-expression -> multiplicative-expression '/' postfix-expression
///   multiplicative-expression -> multiplicative-expression '%' postfix-expression
///   multiplicative-expression -> prefix-expression
void
Parser::parse_multiplicative_expression()
{
  parse_prefix_expression();
  parse_multiplicative_expression_rest();
}

void
Parser::parse_multiplicative_expression_rest()
{
  if (match(Token::star)) {
    parse_prefix_expression();
    parse_multiplicative_expression_rest();
    return;
  }
  if (match(Token::slash)) {
    parse_prefix_expression();
    parse_multiplicative_expression_rest();
    return;
  }
  if (match(Token::percent)) {
    parse_prefix_expression();
    parse_multiplicative_expression_rest();
    return;
  }
}

///   prefix-expression -> '-' prefix-expression
///   prefix-expression -> '/' prefix-expression
///   prefix-expression -> postfix-expression
void
Parser::parse_prefix_expression()
{
  if (match(Token::minus)) {
    parse_prefix_expression();
    return;
  }
  if (match(Token::slash)) {
    parse_prefix_expression();
    return;
  }
  parse_postfix_expression();
}

///   postfix-expression -> postfix-expression '(' argument-list ')'
///   postfix-expression -> primary-expression
void
Parser::parse_postfix_expression()
{
  parse_primary_expression();
  parse_postfix_expression_rest();
}

void
Parser::parse_postfix_expression_rest()
{
  if (match(Token::lparen)) {
    // parse_argument_list();
    expect(Token::rparen);
    parse_postfix_expression_rest();
    return;
  }
}

/// Parse a factor
///
  ///   primary-expression -> integer-literal
  ///   primary-expression -> float-literal
  ///   primary-expression -> identifier
  ///   primary-expression -> '(' factor ')'
void
Parser::parse_primary_expression()
{
  if (match(Token::integer_literal)) {
    return;
  }

  if (match(Token::identifier)) {
    return;
  }

  if (match(Token::lparen)) {
    parse_expression();
    expect(Token::rparen);
    return;
  }

  throw std::runtime_error("expected factor");
}



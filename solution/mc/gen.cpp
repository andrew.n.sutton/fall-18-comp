#include "generator.hpp"

#include <vector>
#include <iterator>
#include <iostream>
#include <fstream>

int
main(int argc, char* argv[])
{
  std::ifstream ifs(argv[1]);
  std::istreambuf_iterator<char> first(ifs);
  std::istreambuf_iterator<char> limit;
  std::string input(first, limit);

  Symbol_table syms;

  Generator parse(syms, input);
  parse.parse_expression();
}




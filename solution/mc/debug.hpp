#pragma once

#include "printer.hpp"

class Debug_printer : public Printer
{
public:
  Debug_printer(std::ostream& os);
};

inline
Debug_printer::Debug_printer
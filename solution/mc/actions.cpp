#include "actions.hpp"
#include "expr.hpp"
#include "stmt.hpp"
#include "type.hpp"

Expr*
Actions::on_boolean_literal(Token const& tok)
{
  if (tok.is(Token::true_kw))
    return m_build.make_true();
  else
    return m_build.make_false();
}

Expr*
Actions::on_integer_literal(Token const& tok)
{
  int n = std::stoi(tok.get_lexeme().str());
  return m_build.make_int(n);
}

Expr*
Actions::on_id_expression(Token const& tok)
{
  Decl* decl = m_stack.lookup(tok.get_lexeme());
  if (!decl)
    throw std::runtime_error("no matching declaration");
  return m_builder.make_id(decl);
}

Expr* 
Actions::on_negation_expression(Expr* arg)
{
  return m_build.make_neg(arg);
}

Expr*
Actions::on_reciprocal_expression(Expr* arg)
{
  return m_build.make_rec(arg);
}

Expr*
Actions::on_multiplication_expression(Expr* e1, Expr* e2)
{
  return m_build.make_mul(e1, e2);
}

Expr*
Actions::on_division_expression(Expr* e1, Expr* e2)
{
  return m_build.make_div(e1, e2);

}

Expr*
Actions::on_remainder_expression(Expr* e1, Expr* e2)
{
  return m_build.make_rem(e1, e2);
}


Decl*
Actions::on_object_declaration(Token id, Type* type)
{
  Scope* scope = get_current_scope();

  // Check for redefinition errors.
  if (scope->lookup(id))
    throw std::runtime_error("redefinition error"); 

  // Partially create the declaration.
  Name* name = m_builder.make_name(id.get_lexeme());
  Decl *var = m_builder.make_variable(name, type);

  // Emit the declaration.
  scope->declare(var);

  return var;
}

void
Actions::finish_object_declaration(Decl* d, Expr* init)
{
  Var_decl* var = static_cast<Var_decl*>(d);

  // Perform copy initialization.
  m_builder.copy_initialize(d, init);
}

Decl*
Actions::on_function_declaration(Token id, std::vector<Decl*> const& parms, Type* type)
{
  Scope* scope = get_current_scope();

  // Check for redefinition errors.
  if (scope->lookup(id))
    throw std::runtime_error("redefinition error"); 

  // Build the function type. For example, if I declare:
  //
  //    fun f(a : int, b : int) -> bool { ... }
  //
  // The function type becomes:
  //
  //    (int, int) -> bool
  Type* ft = m_builder.get_function_type(parms, type);

  // Build the function
  // FIXME: Implement me.
  Fn_decl* fn = nullptr;

  // Emit the declaration.
  scope->declare(var);

  return fn;
}

Decl*
Actions::start_function_declaration(Decl* d)
{
  m_fn = static_cast<Fn_decl*>(d);
}

Decl*
Actions::finish_function_declaration(Decl* d, Stmt* s)
{
  Fn_decl* fn = static_cast<Fn_decl*>(d);

  // Set the body of the function.
  fn->set_body(s);

  m_fn = nullptr;
}


